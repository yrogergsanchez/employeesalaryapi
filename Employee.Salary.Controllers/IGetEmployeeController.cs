﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public interface IGetEmployeeController
    {
        Task<EmployeeViewModel> GetEmployees(PaginationDTO pagination);
    }
}
