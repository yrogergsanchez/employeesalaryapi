﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.Controllers
{
    public static class DependencyContainer
    {
        public static IServiceCollection AddControllerService(
            this IServiceCollection services)
        {
            services.AddScoped<IGenerateEmployeeDataController,
               GenerateEmployeeDataController>();

            services.AddScoped<ICreateEmployeeController,
                CreateEmployeeController>();

            services.AddScoped<IUpdateEmployeeController,
               UpdateEmployeeController>();

            services.AddScoped<IDeleteEmployeeController,
                DeleteEmployeeController>();

            services.AddScoped<IGetEmployeeController,
               GetEmployeeController>();

            services.AddScoped<IGetEmployeeByIdentificationController,
            GetEmployeeByIdentificationController>();

            services.AddScoped<IGetEmployeeByGradeController,
            GetEmployeeByGradeController>();

            services.AddScoped<IGetEmployeeByOfficeAndGradeController,
              GetEmployeeByOfficeAndGradeController>();

            services.AddScoped<IGetEmployeeByPositionAndGradeController,
             GetEmployeeByPositionAndGradeController>();

            services.AddScoped<IGetOfficeController,
             GetOfficeController>();

            services.AddScoped<IGetPositionController,
             GetPositionController>();

            services.AddScoped<IGetDivisionController,
             GetDivisionController>();

            return services;
        }
    }
}
