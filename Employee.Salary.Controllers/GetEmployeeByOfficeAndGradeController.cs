﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.GetEmployeeByOfficeAndGrade;
using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.GetEmployeeByOfficeAndGrade;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public class GetEmployeeByOfficeAndGradeController : IGetEmployeeByOfficeAndGradeController
    {
        readonly IGetEmployeeByOfficeAndGradeInputPort _inputPort;
        readonly IGetEmployeeByOfficeAndGradeOutputPort _outputPort;

        public GetEmployeeByOfficeAndGradeController(IGetEmployeeByOfficeAndGradeInputPort inputPort,
            IGetEmployeeByOfficeAndGradeOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort, outputPort);

        public async Task<EmployeesViewModel> GetEmployeeByOfficeAndGrade(EmployeeByOfficeAndGradeDTO dto)
        {
            await _inputPort.Handle(dto);
            return ((IPresenter<EmployeesViewModel>)_outputPort).Content;
        }
    }
}
