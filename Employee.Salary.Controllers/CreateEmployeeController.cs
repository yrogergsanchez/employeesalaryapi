﻿using Employee.Salary.DTOs.CreateEmployee;
using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.CreateEmployee;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{

    public class CreateEmployeeController: ICreateEmployeeController
    {
        readonly ICreateEmployeeInputPort _inputPort;
        readonly ICreateEmployeeOutputPort _outputPort;

        public CreateEmployeeController(ICreateEmployeeInputPort inputPort,
            ICreateEmployeeOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort, outputPort);

        public async Task<CreateEmployeeViewModel> CreateEmployee(List<CreateEmployeeDTO> employees)
        {
            await _inputPort.Handle(employees);
            return ((IPresenter<CreateEmployeeViewModel>)_outputPort).Content;
        }
    }
}
