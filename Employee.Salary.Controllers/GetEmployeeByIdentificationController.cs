﻿using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.GetEmployeeByIdentification;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public class GetEmployeeByIdentificationController : IGetEmployeeByIdentificationController
    {
        readonly IGetEmployeeByIdentificationInputPort _inputPort;
        readonly IGetEmployeeByIdentificationOutputPort _outputPort;

        public GetEmployeeByIdentificationController(IGetEmployeeByIdentificationInputPort inputPort,
            IGetEmployeeByIdentificationOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort, outputPort);

        public async Task<EmployeeByIdentificationViewModel> GetEmployeeByIdentification(string identification)
        {
            await _inputPort.Handle(identification);
            return ((IPresenter<EmployeeByIdentificationViewModel>)_outputPort).Content;
        }
    }
}
