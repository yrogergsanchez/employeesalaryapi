﻿using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.GetOffices;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    class GetOfficeController : IGetOfficeController
    {
        readonly IGetOfficeInputPort _inputPort;
        readonly IGetOfficeOutputPort _outputPort;

        public GetOfficeController(IGetOfficeInputPort inputPort,
            IGetOfficeOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort, outputPort);


        public async Task<OfficeViewModel> GetOffices()
        {
            await _inputPort.Handle();
            return ((IPresenter<OfficeViewModel>)_outputPort).Content;
        }

    }
}
