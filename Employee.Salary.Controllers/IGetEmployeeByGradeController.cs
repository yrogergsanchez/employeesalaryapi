﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.GetEmployeeByGrade;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public interface IGetEmployeeByGradeController
    {
        Task<EmployeesViewModel> GetEmployeeByGrade(EmployeeByGradeDTO dto);
    }
}
