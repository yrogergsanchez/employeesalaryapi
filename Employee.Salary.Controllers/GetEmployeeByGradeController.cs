﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.GetEmployeeByGrade;
using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.GetEmployeeByGrade;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public class GetEmployeeByGradeController : IGetEmployeeByGradeController
    {
        readonly IGetEmployeeByGradeInputPort _inputPort;
        readonly IGetEmployeeByGradeOutputPort _outputPort;

        public GetEmployeeByGradeController(IGetEmployeeByGradeInputPort inputPort,
            IGetEmployeeByGradeOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort, outputPort);
              
        public async Task<EmployeesViewModel> GetEmployeeByGrade(EmployeeByGradeDTO dto)
        {
            await _inputPort.Handle(dto);
            return ((IPresenter<EmployeesViewModel>)_outputPort).Content;
        }
    }
}
