﻿using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.DeleteEmployee;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public class DeleteEmployeeController : IDeleteEmployeeController
    {
        readonly IDeleteEmployeeInputPort _inputPort;
        readonly IDeleteEmployeeOutputPort _outputPort;

        public DeleteEmployeeController(IDeleteEmployeeInputPort inputPort,
            IDeleteEmployeeOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort, outputPort);


        public async Task<DeleteEmployeeViewModel> DeleteEmployee(int idEmployee)
        {
            await _inputPort.Handle(idEmployee);
            return ((IPresenter<DeleteEmployeeViewModel>)_outputPort).Content;
        }
    }
}
