﻿using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public interface IGetEmployeeByIdentificationController
    {
        Task<EmployeeByIdentificationViewModel> GetEmployeeByIdentification(string identification);
    }
}
