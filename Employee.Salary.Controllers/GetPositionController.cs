﻿using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.GetPositions;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public class GetPositionController : IGetPositionController
    {
        readonly IGetPositionInputPort _inputPort;
        readonly IGetPositionOutputPort _outputPort;

        public GetPositionController(IGetPositionInputPort inputPort,
            IGetPositionOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort, outputPort);


        public async Task<PositionViewModel> GetPositions()
        {
            await _inputPort.Handle();
            return ((IPresenter<PositionViewModel>)_outputPort).Content;
        }

    }
}
