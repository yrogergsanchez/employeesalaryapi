﻿using Employee.Salary.DTOs.UpdateEmployee;
using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.UpdateEmployee;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public class UpdateEmployeeController : IUpdateEmployeeController
    {
        readonly IUpdateEmployeeInputPort _inputPort;
        readonly IUpdateEmployeeOutputPort _outputPort;

        public UpdateEmployeeController(IUpdateEmployeeInputPort inputPort,
            IUpdateEmployeeOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort, outputPort);

        public async Task<UpdateEmployeeViewModel> UpdateEmployee(List<UpdateEmployeeDTO> employees)
        {
            await _inputPort.Handle(employees);
            return ((IPresenter<UpdateEmployeeViewModel>)_outputPort).Content;
        }
    }
}
