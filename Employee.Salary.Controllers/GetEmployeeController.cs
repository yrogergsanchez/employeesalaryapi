﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.GetEmployees;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public class GetEmployeeController : IGetEmployeeController
    {
        readonly IGetEmployeesInputPort _inputPort;
        readonly IGetEmployeesOutputPort _outputPort;

        public GetEmployeeController(IGetEmployeesInputPort inputPort,
            IGetEmployeesOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort, outputPort);

       

        public async Task<EmployeeViewModel> GetEmployees(PaginationDTO pagination)
        {
            await _inputPort.Handle(pagination);
            return ((IPresenter<EmployeeViewModel>)_outputPort).Content;
        }
    }
}
