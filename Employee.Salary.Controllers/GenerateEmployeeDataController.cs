﻿using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.GenerateEmployeeData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public class GenerateEmployeeDataController : IGenerateEmployeeDataController
    {       
        readonly IGenerateEmployeeDataInputPort _inputPort;
        readonly IGenerateEmployeeDataOutputPort _outputPort;

        public GenerateEmployeeDataController(IGenerateEmployeeDataInputPort inputPort,
            IGenerateEmployeeDataOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort, outputPort);

        public async Task GenerateEmployeeData()
        {
            await _inputPort.Handle();           
        }
    }
}
