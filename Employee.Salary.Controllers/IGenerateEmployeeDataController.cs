﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public interface IGenerateEmployeeDataController
    {
        Task GenerateEmployeeData();
    }
}
