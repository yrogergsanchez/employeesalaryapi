﻿using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.GetDivisions;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public class GetDivisionController : IGetDivisionController
    {
        readonly IGetDivisionInputPort _inputPort;
        readonly IGetDivisionOutputPort _outputPort;

        public GetDivisionController(IGetDivisionInputPort inputPort,
            IGetDivisionOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort, outputPort);


        public async Task<DivisionViewModel> GetDivisions()
        {
            await _inputPort.Handle();
            return ((IPresenter<DivisionViewModel>)_outputPort).Content;
        }

    }
}
