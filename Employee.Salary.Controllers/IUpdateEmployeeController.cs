﻿using Employee.Salary.DTOs.UpdateEmployee;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public interface IUpdateEmployeeController
    {
        Task<UpdateEmployeeViewModel> UpdateEmployee(List<UpdateEmployeeDTO> employees);
    }
}
