﻿using Employee.Salary.DTOs.CreateEmployee;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public interface ICreateEmployeeController
    {
        Task<CreateEmployeeViewModel> CreateEmployee(List<CreateEmployeeDTO> employee);
    }
}
