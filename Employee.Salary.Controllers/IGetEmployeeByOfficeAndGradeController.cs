﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.GetEmployeeByOfficeAndGrade;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public interface IGetEmployeeByOfficeAndGradeController
    {
        Task<EmployeesViewModel> GetEmployeeByOfficeAndGrade(EmployeeByOfficeAndGradeDTO dto);
    }
}
