﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.GetEmployeeByOfficeAndGrade;
using Employee.Salary.DTOs.GetEmployeeByPositionAndGrade;
using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.GetEmployeeByOfficeAndGrade;
using Employee.Salary.UseCasesPorts.GetEmployeeByPositionAndGrade;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    public class GetEmployeeByPositionAndGradeController : IGetEmployeeByPositionAndGradeController
    {
        readonly IGetEmployeeByPositionAndGradeInputPort _inputPort;
        readonly IGetEmployeeByPositionAndGradeOutputPort _outputPort;

        public GetEmployeeByPositionAndGradeController(IGetEmployeeByPositionAndGradeInputPort inputPort,
            IGetEmployeeByPositionAndGradeOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort, outputPort);

        public async Task<EmployeesViewModel> GetEmployeeByPositionAndGrade(EmployeeByPositionAndGradeDTO dto)
        {
            await _inputPort.Handle(dto);
            return ((IPresenter<EmployeesViewModel>)_outputPort).Content;
        }

    }
}
