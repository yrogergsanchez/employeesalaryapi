﻿using Employee.Salary.UseCasesPorts.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCasesPorts.DeleteEmployee
{
    public interface IDeleteEmployeeInputPort
        : IPorts<int>
    {
    }
}
