﻿using Employee.Salary.DTOs.GetEmployeeByPositionAndGrade;
using Employee.Salary.UseCasesPorts.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCasesPorts.GetEmployeeByPositionAndGrade
{
    public interface IGetEmployeeByPositionAndGradeInputPort
        : IPorts<EmployeeByPositionAndGradeDTO>
    {
    }
}
