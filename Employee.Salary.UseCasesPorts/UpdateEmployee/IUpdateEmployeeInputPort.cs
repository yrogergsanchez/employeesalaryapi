﻿using Employee.Salary.DTOs.UpdateEmployee;
using Employee.Salary.UseCasesPorts.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCasesPorts.UpdateEmployee
{
    public interface IUpdateEmployeeInputPort
        : IPorts<List<UpdateEmployeeDTO>>
    {
    }
}
