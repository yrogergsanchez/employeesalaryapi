﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.UseCasesPorts.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCasesPorts.GetEmployees
{
    public interface IGetEmployeesInputPort
        : IPorts<PaginationDTO>
    {
    }
}
