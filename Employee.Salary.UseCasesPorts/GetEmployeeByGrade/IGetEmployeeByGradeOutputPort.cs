﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.UseCasesPorts.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCasesPorts.GetEmployeeByGrade
{
    public interface IGetEmployeeByGradeOutputPort :
        IPorts<IEnumerable<EmployeeAllDTO>>
    {
    }
}
