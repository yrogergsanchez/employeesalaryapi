﻿using Employee.Salary.DTOs.GetEmployeeByGrade;
using Employee.Salary.UseCasesPorts.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCasesPorts.GetEmployeeByGrade
{
    public interface IGetEmployeeByGradeInputPort
        : IPorts<EmployeeByGradeDTO>
    {
    }
}
