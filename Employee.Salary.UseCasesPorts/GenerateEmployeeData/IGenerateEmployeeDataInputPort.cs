﻿using Employee.Salary.UseCasesPorts.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCasesPorts.GenerateEmployeeData
{
    public interface IGenerateEmployeeDataInputPort
         : IPorts
    {
    }
}
