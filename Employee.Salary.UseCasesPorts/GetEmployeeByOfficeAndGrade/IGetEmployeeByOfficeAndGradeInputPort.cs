﻿using Employee.Salary.DTOs.GetEmployeeByOfficeAndGrade;
using Employee.Salary.UseCasesPorts.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCasesPorts.GetEmployeeByOfficeAndGrade
{
    public interface IGetEmployeeByOfficeAndGradeInputPort
        : IPorts<EmployeeByOfficeAndGradeDTO>
    {
    }
}
