﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.GetEmployeeByIdentification;
using Employee.Salary.UseCasesPorts.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCasesPorts.GetEmployeeByIdentification
{
    public interface IGetEmployeeByIdentificationOutputPort
        : IPorts<EmployeeByIdentificationDTO>
    {
    }
}
