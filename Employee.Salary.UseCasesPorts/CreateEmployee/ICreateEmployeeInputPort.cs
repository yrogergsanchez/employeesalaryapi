﻿using Employee.Salary.DTOs.CreateEmployee;
using Employee.Salary.UseCasesPorts.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCasesPorts.CreateEmployee
{
    public interface ICreateEmployeeInputPort
        :  IPorts<List<CreateEmployeeDTO>>
    {
    }
}
