﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.DTOs.GetEmployeeByPositionAndGrade
{
    public class EmployeeByPositionAndGradeDTO
    {
        public string Position { get; set; }
        public int Grade { get; set; }
        public PaginationDTO Pagination { get; set; }
    }
}
