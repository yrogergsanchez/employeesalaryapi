﻿using Employee.Salary.Entities.Validators;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Employee.Salary.DTOs.CreateEmployee
{
    public class CreateEmployeeDTOValidator
        : AbstractValidator<CreateEmployeeDTO>,
        Entities.Validators.IValidator<CreateEmployeeDTO>
    {

        public CreateEmployeeDTOValidator()
        {
            RuleFor(x => x.Year)
                .GreaterThan(0)
                .WithMessage("Debe proporcionar el año del salario del empleado");

            RuleFor(x => x.Month)
                .GreaterThan(0)
                .WithMessage("Debe proporcionar el mes del salario del empleado");

            RuleFor(x => x.EmployeeCode)
                .NotEmpty()
                .WithMessage("Debe proporcionar el identificador del empleado");

            RuleFor(x => x.EmployeeName)
               .NotEmpty()
               .WithMessage("Debe proporcionar el nombre del empleado");

            RuleFor(x => x.EmployeeSurname)
               .NotEmpty()
               .WithMessage("Debe proporcionar el apellido del empleado");

            RuleFor(x => x.Grade)
               .GreaterThan(0)
               .WithMessage("Debe proporcionar el grado del empleado");

            RuleFor(x => x.IdentificationNumber)
               .NotEmpty()
              .WithMessage("Debe proporcionar la identificación del empleado");

            RuleFor(x => x.BeginDate)              
               .NotEmpty()
               .NotNull()
               .Must(date => date != default(DateTime))
              .WithMessage("Debe proporcionar la fecha de inicio del empleado");

            RuleFor(x => x.Birthday)
               .NotEmpty()
               .NotNull()
               .Must(date => date != default(DateTime))
              .WithMessage("Debe proporcionar la fecha de cumpleaño del empleado");

        }
        
        ValidationResult Entities.Validators.IValidator<CreateEmployeeDTO>.Validate(CreateEmployeeDTO instance)
        {
            var result = Validate(instance);
            return new ValidationResult(
                result.Errors?
                .Select(e =>
                    new ValidationFailure(e.PropertyName, e.ErrorMessage)));
        }
    }
}
