﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.DTOs.Common
{
    public class PaginationDTO
    {
        public string Search { get; set; }
        public int Page { get; set; } = 1;
        public int Length { get; set; } = 10;
    }
}
