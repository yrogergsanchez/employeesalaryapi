﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.DTOs.Common
{
    public class DivisionDTO
    {
        public string Name { get; set; }
    }
}
