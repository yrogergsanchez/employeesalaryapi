﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.DTOs.GetEmployeeByOfficeAndGrade
{
    public class EmployeeByOfficeAndGradeDTO
    {
        public string Office { get; set; }
        public int Grade { get; set; }
        public PaginationDTO Pagination { get; set; }
    }
}
