﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.DTOs.GetEmployeeByGrade
{
    public class EmployeeByGradeDTO
    {
        public int Grade { get; set; }
        public PaginationDTO Pagination { get; set; }
    }
}
