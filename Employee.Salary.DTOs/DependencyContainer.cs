﻿using Employee.Salary.DTOs.CreateEmployee;
using Employee.Salary.Entities.Validators;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.DTOs
{
    public static class DependencyContainer
    {
        public static IServiceCollection AddValidators(
           this IServiceCollection services)
        {
            services.AddScoped<IValidator<CreateEmployeeDTO>,
                CreateEmployeeDTOValidator>();                        

            return services;
        }
    }
}
