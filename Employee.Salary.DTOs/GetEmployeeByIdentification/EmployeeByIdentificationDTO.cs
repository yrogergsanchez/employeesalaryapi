﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.DTOs.GetEmployeeByIdentification
{
    public class EmployeeByIdentificationDTO
    {
        public int Id { get; set; }
        public string Office { get; set; } = "";       
        public string EmployeeCode { get; set; } = "";
        public string EmployeeName { get; set; } = "";
        public string EmployeeSurname { get; set; } = "";
        public string Division { get; set; } = "";
        public string Position { get; set; } = "";
        public int Grade { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public string IdentificationNumber { get; set; } = "";
        public string Payments { get; set; }
    }
}
