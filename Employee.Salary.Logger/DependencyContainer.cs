﻿using Employee.Salary.Entities.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.Logger
{
    public static class DependencyContainer
    {
        public static IServiceCollection AddLogger(
            this IServiceCollection services)
        {
            services.AddScoped<IApplicationStatusLogger, DebugStatusLogger>();

            return services;
        }
    }
}
