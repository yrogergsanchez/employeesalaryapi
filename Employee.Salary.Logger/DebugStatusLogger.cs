﻿using Employee.Salary.Entities.Interfaces;
using System;
using System.Diagnostics;

namespace Employee.Salary.Logger
{
    public class DebugStatusLogger : IApplicationStatusLogger
    {
        public void Log(string message)
        {
            Debug.WriteLine($"*** DSL: {message}");
        }
    }
}
