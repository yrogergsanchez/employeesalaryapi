﻿using Employee.Salary.Sales.WebExceptionPresenters.ExceptionHandlers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.WebExceptionPresenters.Filters
{
    public static class Filters
    {
        public static void Register(MvcOptions option)
        {
            option.Filters.Add(new WebExceptionFilterAttribute(
                new ExceptionService()));
        }
    }
}
