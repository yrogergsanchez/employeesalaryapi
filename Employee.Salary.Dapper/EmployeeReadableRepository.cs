﻿using Dapper;
using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.GetEmployeeByIdentification;
using Employee.Salary.Entities.Specification;
using Employee.Salary.LINQExpressionToSQL;
using Employee.Salary.UseCases.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Employee.Salary.Dapper
{
    public class EmployeeReadableRepository : IEmployeeReadableRepository
    {
        readonly IDbConnection _connection;
        public EmployeeReadableRepository(IDbConnection connection)
        {
            _connection = connection;
        }


        public async Task<Employee.Salary.Entities.POCOs.Employee> GetEmployeeById(int idEmployee)
        {
            string sql = @$"SELECT * FROM Empleados WHERE Id = {idEmployee}";

            _connection.Open();
            var employee = await _connection.QueryFirstOrDefaultAsync<Employee.Salary.Entities.POCOs.Employee>(sql);
            _connection.Close();

            return employee;
        }


        public async Task<EmployeeByIdentificationDTO> GetEmployeeByIdentification(string identification)
        {
            try
            {
                string fields = $@"Id,
	                          Office, 
	                          EmployeeCode=Employee_Code, 
	                          EmployeeName=Employee_Name, 
	                          EmployeeSurname=Employee_Surname, 
                              IdentificationNumber=Identification_Number,  
	                          Division, 
	                          Position, 
	                          Grade, 
	                          Begin_Date, 
	                          Birthday,
	                          Payments = (SELECT 
		                        id, 
		                        year, 
		                        month, 
		                        baseSalary = Base_Salary, 
		                        productionBonus = Production_Bonus, 
		                        compensationBonus =Compensation_Bonus, 
		                        commission, 
		                        contributions 
                                FROM Empleados 
                                WHERE Identification_Number = '{identification}' 
                                FOR JSON PATH)";
                string tables = @"Empleados";
                string where = $"Identification_Number = '{identification}'";
                string orderBy = "Year DESC, Month DESC";

                string sql = @$"SELECT {fields} FROM {tables} WHERE {where} ORDER BY {orderBy}";
                _connection.Open();

                var employee = await _connection.QueryFirstOrDefaultAsync<EmployeeByIdentificationDTO>(sql);


                _connection.Close();

                return employee;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public IEnumerable<EmployeeDTO> GetEmployees(PaginationDTO paginationDTO)
        {
            string ctes = @"Employee_Code, Max(Year) Year, Max(Month) Month";
            string groupBy = "Group by Employee_Code";
            string fields = @"EmployeeCode = E.Employee_Code, 
	                          EmployeeName = E.Employee_Name,
	                          EmployeeSurname = E.Employee_Surname,
	                          E.Division, 
                              E.Office,
	                          E.Position, 
	                          E.Grade, 
	                          BeginDate=E.Begin_Date, 
	                          E.Birthday, 
	                          IdentificationNumber=E.Identification_Number,
	                          TotalSalary = (E.Base_Salary + E.Production_Bonus + ((E.Base_Salary * 0.8 / 100) + E.Commission) + (E.Compensation_Bonus * 0.75 / 100))";
            string tables = @"Empleados E Inner Join Cte ct On E.Employee_Code = E.Employee_Code And E.Year = Ct.Year And E.Month = Ct.Month";
            string orderBy = "Order By 1 DESC";
            string pagination = $"OFFSET {paginationDTO.Length} * ({paginationDTO.Page} - 1) ROWS FETCH NEXT {paginationDTO.Length} ROWS ONLY;";
            string sql = @$"With Cte As (SELECT {ctes} FROM Empleados {groupBy}) SELECT DISTINCT {fields} FROM {tables} {orderBy} {pagination}";
            _connection.Open();
            IEnumerable<EmployeeDTO> employees = _connection.Query<EmployeeDTO>(sql);
            _connection.Close();

            return employees;
        }

        public IEnumerable<EmployeeDTO> GetAllEmployees()
        {
            string fields = @"Year, 
                             Month,
                             Office, 
                             EmployeeCode=Employee_Code, 
                             EmployeeName=Employee_Name, 
                             EmployeeSurname=Employee_Surname, 
                             Division, 
                             Position, 
                             Grade, 
                             Begin_Date, 
                             Birthday, 
                             IdentificationNumber=Identification_Number, 
                             BaseSalary=Base_Salary,
                             ProductionBonus=Production_Bonus, 
                             CompensationBonus=Compensation_Bonus, 
                             Commission, 
                             Contributions";
            string tables = @"Empleados";

            string sql = @$"SELECT {fields} FROM {tables}";
            _connection.Open();
            IEnumerable<EmployeeDTO> employees = _connection.Query<EmployeeDTO>(sql);
            _connection.Close();

            return employees;
        }

        public IEnumerable<EmployeeAllDTO> GetEmployeesBySpecification(Specification<EmployeeAllDTO> specification, PaginationDTO paginationDTO)
        {
            string ctes = @"Employee_Code, Max(Year) Year, Max(Month) Month";
            string groupBy = "Group by Employee_Code";
            string fields = @"EmployeeCode=E.Employee_Code, 
                              EmployeeName=Employee_Name, 
                              EmployeeSurname=Employee_Surname, 
	                          E.Division, 
	                          E.Position, 
                              E.Office,
	                          E.Grade, 
	                          E.Begin_Date, 
	                          E.Birthday, 
	                          IdentificationNumber=E.Identification_Number, 
	                          TotalSalary = (E.Base_Salary + E.Production_Bonus + ((E.Base_Salary * 0.8 / 100) + E.Commission) + (E.Compensation_Bonus * 0.75 / 100))";
            string tables = @"Empleados E Inner Join Cte ct On E.Employee_Code = E.Employee_Code And E.Year = Ct.Year And E.Month = Ct.Month";
            string orderBy = "Order By 1 DESC";
            string pagination = $"OFFSET {paginationDTO.Length} * ({paginationDTO.Page} - 1) ROWS FETCH NEXT {paginationDTO.Length} ROWS ONLY;";
           

            Translator translator = new Translator();
            string where = translator.Translate(specification,
                specification.ConditionExpression);           

            string sql = @$"With Cte As (SELECT  {ctes} FROM Empleados {groupBy}) SELECT DISTINCT {fields} FROM {tables} WHERE {where} {orderBy} {pagination}";
            _connection.Open();
            IEnumerable<EmployeeAllDTO> employees = _connection.Query<EmployeeAllDTO>(sql);
            _connection.Close();

            return employees;
        }
    }
}
