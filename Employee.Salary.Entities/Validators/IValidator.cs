﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.Entities.Validators
{
    public interface IValidator<T>
    {
        ValidationResult Validate(T instance);
    }
}
