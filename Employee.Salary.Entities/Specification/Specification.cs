﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Entities.Specification
{
    public abstract class Specification<T>
    {
        public abstract Expression<Func<T, bool>> ConditionExpression { get; }
        public bool IsSatisfiedBy(T entity)
        {
            Func<T, bool> expressionDelegate = ConditionExpression.Compile();
            return expressionDelegate(entity);
        }

        public Specification<T> And(Specification<T> specification) =>
           new AndSpecification<T>(this, specification);
               
    }
}
