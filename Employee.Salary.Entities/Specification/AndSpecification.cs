﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Entities.Specification
{
    public class AndSpecification<T> : Specification<T>
    {
        // S1 && S2
        readonly Specification<T> Left;
        readonly Specification<T> Right;

        public AndSpecification(Specification<T> left, Specification<T> right) =>
            (Left, Right) = (left, right);

        public override Expression<Func<T, bool>> ConditionExpression
        {
            get
            {
                var Param = System.Linq.Expressions.Expression
                    .Parameter(typeof(T));

                var Body = System.Linq.Expressions.Expression
                    .AndAlso(
                    System.Linq.Expressions.Expression.Invoke(
                        Left.ConditionExpression, Param),
                    System.Linq.Expressions.Expression.Invoke(
                        Right.ConditionExpression, Param)
                    );
                return System.Linq.Expressions.Expression
                    .Lambda<Func<T, bool>>(Body, Param);
            }
        }
    }
}
