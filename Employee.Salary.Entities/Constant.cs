﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Employee.Salary.Entities
{
    public class Constant
    {
        public static readonly string JsonPathDivision
           = Path.Combine(Environment.CurrentDirectory, @"Data\divisions.json");

        public static readonly string JsonPathOffice
          = Path.Combine(Environment.CurrentDirectory, @"Data\offices.json");

        public static readonly string JsonPathPosition
          = Path.Combine(Environment.CurrentDirectory, @"Data\positions.json");

    }
}
