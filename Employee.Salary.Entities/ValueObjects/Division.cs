﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.Entities.ValueObjects
{
    public class Division
    {
        public string Name { get; set; }
    }
}
