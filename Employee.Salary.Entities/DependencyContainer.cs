﻿using Employee.Salary.Entities.Validators;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.Entities
{
    public static class DependencyContainer
    {
        public static IServiceCollection AddEntityServices(
            this IServiceCollection services)
        {
           
            services.AddSingleton(typeof(IValidatorService<>),
                typeof(ValidatorService<>));
            return services;
        }
    }
}
