﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.Entities.Interfaces
{
    public interface IFileReader
    {
        string GetFileAsString(string filepath);
    }
}
