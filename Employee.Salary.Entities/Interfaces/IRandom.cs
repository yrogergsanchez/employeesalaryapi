﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.Entities.Interfaces
{
    public interface IRandom
    {
        string GenerateName();
        string GenerateNumber(int len);
        DateTime GenerateDate(DateTime start, DateTime end);
        IList<DateTime> GetRandomDates(DateTime startDate, DateTime maxDate, int range);
        string GetNamesByArrayString(string[] arrays);
        int GetIndex(int length);
        decimal GetAmount();
        decimal GetAmount(int min, int max);
    }
}
