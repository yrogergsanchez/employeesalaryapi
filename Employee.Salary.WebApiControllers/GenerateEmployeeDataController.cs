﻿using Employee.Salary.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.WebApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenerateEmployeeDataController : ControllerBase
    {
        readonly IGenerateEmployeeDataController _controller;
        public GenerateEmployeeDataController(IGenerateEmployeeDataController controller)
            => (_controller) = (controller);

        [HttpPost("generate")]
        public async Task<IActionResult> GenerateEmployeeData()
        {
            await _controller.GenerateEmployeeData();
            return Ok();
        }
    }
}
