﻿using Employee.Salary.DTOs.CreateEmployee;
using Employee.Salary.Presenters;
using Employee.Salary.UseCasesPorts.CreateEmployee;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CreateEmployeeController : ControllerBase
    {
        readonly ICreateEmployeeController _controller;
        public CreateEmployeeController(ICreateEmployeeController controller)
            => (_controller) = (controller);

        [HttpPost("create")]
        public async Task<IActionResult> CreateEmployee(
            List<CreateEmployeeDTO> employees)
        {
            var createEmployeeViewModel =
                 await _controller.CreateEmployee(employees);
            return Ok(createEmployeeViewModel.EmployeeIds);
        }
    }
}
