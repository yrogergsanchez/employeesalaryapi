﻿using Employee.Salary.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.WebApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetOfficeController : ControllerBase
    {
        readonly IGetOfficeController _controller;
        public GetOfficeController(IGetOfficeController controller)
            => (_controller) = (controller);

        [HttpGet("get-all")]
        public async Task<IActionResult> GetOffices()
        {
            var employeeViewModel =
                 await _controller.GetOffices();
            return Ok(employeeViewModel.Offices);
        }
    }
}
