﻿using Employee.Salary.Controllers;
using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.GetEmployeeByPositionAndGrade;
using Employee.Salary.Presenters;
using Employee.Salary.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.WebApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetEmployeeByPositionAndGradeController : ControllerBase
    {
        readonly IGetEmployeeByPositionAndGradeController _controller;
        public GetEmployeeByPositionAndGradeController(IGetEmployeeByPositionAndGradeController controller)
            => (_controller) = (controller);


        [HttpPost("get-employees-by-position-and-grade")]
        public async Task<IActionResult> GetEmployeeByPositionAndGrade(EmployeeByPositionAndGradeDTO dto)
        {
            var employeeViewModel =
                  await _controller.GetEmployeeByPositionAndGrade(dto);
            return Ok(employeeViewModel.Employees);
        }

    }
}
