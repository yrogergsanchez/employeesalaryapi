﻿using Employee.Salary.Controllers;
using Employee.Salary.DTOs.GetEmployeeByOfficeAndGrade;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.WebApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetEmployeeByOfficeAndGradeController : ControllerBase
    {
        readonly IGetEmployeeByOfficeAndGradeController _controller;
        public GetEmployeeByOfficeAndGradeController(IGetEmployeeByOfficeAndGradeController controller)
            => (_controller) = (controller);


        [HttpGet("{grade}/{office}")]
        public async Task<IActionResult> GetEmployeeByPositionAndGrade(int grade, string office)
        {
            var employeeViewModel =
                  await _controller.GetEmployeeByOfficeAndGrade(new EmployeeByOfficeAndGradeDTO { Grade = grade, Office = office, Pagination = new DTOs.Common.PaginationDTO { Page = 1, Length = 10 } });
            return Ok(employeeViewModel.Employees);
        }
    }
}
