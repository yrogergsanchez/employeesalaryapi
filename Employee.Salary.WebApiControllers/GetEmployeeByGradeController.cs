﻿using Employee.Salary.Controllers;
using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.GetEmployeeByGrade;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.WebApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetEmployeeByGradeController : ControllerBase
    {
        readonly IGetEmployeeByGradeController _controller;
        public GetEmployeeByGradeController(IGetEmployeeByGradeController controller)
            => (_controller) = (controller);


        [HttpPost("get-employees-by-grade")]
        public async Task<IActionResult> GetEmployeeByGrade(EmployeeByGradeDTO dto)
        {
            var employeeViewModel =
                  await _controller.GetEmployeeByGrade(dto);
            return Ok(employeeViewModel.Employees);
        }
    }
}
