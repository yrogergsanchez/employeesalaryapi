﻿using Employee.Salary.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.WebApiControllers
{
 

    [Route("api/[controller]")]
    [ApiController]
    public class GetEmployeeByIdentificationController : ControllerBase
    {
        readonly IGetEmployeeByIdentificationController _controller;
        public GetEmployeeByIdentificationController(IGetEmployeeByIdentificationController controller)
            => (_controller) = (controller);

        [HttpGet("{identification}")]
        public async Task<IActionResult> GetEmployeeByIdentification(string identification)
        {
            var employeeViewModel =
                 await _controller.GetEmployeeByIdentification(identification);
            return Ok(employeeViewModel.Employee);
        }
    }

}
