﻿using Employee.Salary.Controllers;
using Employee.Salary.DTOs.UpdateEmployee;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.WebApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UpdateEmployeeController : ControllerBase
    {
        readonly IUpdateEmployeeController _controller;
        public UpdateEmployeeController(IUpdateEmployeeController controller)
            => (_controller) = (controller);

        [HttpPut("update/{id}")]
        public async Task<IActionResult> UpdateEmployee(int id,
            List<UpdateEmployeeDTO> employees)
        {
            var createEmployeeViewModel =
                 await _controller.UpdateEmployee(employees);
            return Ok();
        }
    }

}
