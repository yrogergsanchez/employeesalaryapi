﻿using Employee.Salary.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.WebApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeleteEmployeeController : ControllerBase
    {
        readonly IDeleteEmployeeController _controller;
        public DeleteEmployeeController(IDeleteEmployeeController controller)
            => (_controller) = (controller);

        [HttpDelete("delete")]
        public async Task<IActionResult> UpdateEmployee([FromBody] int id)
        {
            await _controller.DeleteEmployee(id);
            return Ok();
        }
    }
}
