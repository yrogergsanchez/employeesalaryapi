﻿using Employee.Salary.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.WebApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetDivisionController : ControllerBase
    {
        readonly IGetDivisionController _controller;
        public GetDivisionController(IGetDivisionController controller)
            => (_controller) = (controller);

        [HttpGet("get-all")]
        public async Task<IActionResult> GetDivisions()
        {
            var employeeViewModel =
                 await _controller.GetDivisions();
            return Ok(employeeViewModel.Divisions);
        }
    }
}
