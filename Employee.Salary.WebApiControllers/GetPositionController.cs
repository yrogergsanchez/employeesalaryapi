﻿using Employee.Salary.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.WebApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetPositionController : ControllerBase
    {
        readonly IGetPositionController _controller;
        public GetPositionController(IGetPositionController controller)
            => (_controller) = (controller);

        [HttpGet("get-all")]
        public async Task<IActionResult> GetPositions()
        {
            var employeeViewModel =
                 await _controller.GetPositions();
            return Ok(employeeViewModel.Positions);
        }
    }
}
