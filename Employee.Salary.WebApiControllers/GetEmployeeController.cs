﻿using Employee.Salary.Controllers;
using Employee.Salary.DTOs.Common;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.WebApiControllers
{   
    [Route("api/[controller]")]
    [ApiController]
    public class GetEmployeeController : ControllerBase
    {
        readonly IGetEmployeeController _controller;
        public GetEmployeeController(IGetEmployeeController controller)
            => (_controller) = (controller);

        [HttpPost("get-all")]
        public async Task<IActionResult> GetEmployees(PaginationDTO pagination)
        {
            var employeeViewModel =
                 await _controller.GetEmployees(pagination);
            return Ok(employeeViewModel.Employees);
        }
    }
}
