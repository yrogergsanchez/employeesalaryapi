﻿using Employee.Salary.UseCasesPorts.DeleteEmployee;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Presenters
{
    class DeleteEmployeePresenter : IDeleteEmployeeOutputPort, IPresenter<DeleteEmployeeViewModel>
    {
        public DeleteEmployeeViewModel Content { get; private set; }
        public Task Handle()
        {
            Content = new DeleteEmployeeViewModel();
            return Task.CompletedTask;
        }
    }
}
