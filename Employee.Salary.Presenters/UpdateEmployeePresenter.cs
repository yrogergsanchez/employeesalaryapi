﻿using Employee.Salary.UseCasesPorts.UpdateEmployee;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Presenters
{    
    public class UpdateEmployeePresenter : IUpdateEmployeeOutputPort, IPresenter<UpdateEmployeeViewModel>
    {
        public UpdateEmployeeViewModel Content { get; private set; }
        public Task Handle()
        {
            Content = new UpdateEmployeeViewModel();
            return Task.CompletedTask;
        }
    }
}
