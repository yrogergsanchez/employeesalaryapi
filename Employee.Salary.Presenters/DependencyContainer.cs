﻿using Employee.Salary.UseCasesPorts.CreateEmployee;
using Employee.Salary.UseCasesPorts.DeleteEmployee;
using Employee.Salary.UseCasesPorts.GenerateEmployeeData;
using Employee.Salary.UseCasesPorts.GetDivisions;
using Employee.Salary.UseCasesPorts.GetEmployeeByGrade;
using Employee.Salary.UseCasesPorts.GetEmployeeByIdentification;
using Employee.Salary.UseCasesPorts.GetEmployeeByOfficeAndGrade;
using Employee.Salary.UseCasesPorts.GetEmployeeByPositionAndGrade;
using Employee.Salary.UseCasesPorts.GetEmployees;
using Employee.Salary.UseCasesPorts.GetOffices;
using Employee.Salary.UseCasesPorts.GetPositions;
using Employee.Salary.UseCasesPorts.UpdateEmployee;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.Presenters
{
    public static class DependencyContainer
    {
        public static IServiceCollection AddPresenters(this IServiceCollection services)
        {
            services.AddScoped<ICreateEmployeeOutputPort,
                CreateEmployeePresenter>();

            services.AddScoped<IUpdateEmployeeOutputPort,
               UpdateEmployeePresenter>();

            services.AddScoped<IDeleteEmployeeOutputPort,
               DeleteEmployeePresenter>();

            services.AddScoped<IGetEmployeesOutputPort,
               GetEmployeePresenter>();

            services.AddScoped<IGetEmployeeByIdentificationOutputPort,
               GetEmployeeByIdentificationPresenter>();

            services.AddScoped<IGenerateEmployeeDataOutputPort,
               GenerateEmployeeDataPresenter>();

            services.AddScoped<IGetDivisionOutputPort,
              GetDivisionPresenter>();

            services.AddScoped<IGetOfficeOutputPort,
              GetOfficePresenter>();

            services.AddScoped<IGetPositionOutputPort,
              GetPositionPresenter>();

            services.AddScoped<IGetEmployeeByGradeOutputPort,
              GetEmployeesByGradePresenter>();

            services.AddScoped<IGetEmployeeByOfficeAndGradeOutputPort,
              GetEmployeesByOfficeAndGradePresenter>();

            services.AddScoped<IGetEmployeeByPositionAndGradeOutputPort,
              GetEmployeesByPositionAndGradePresenter>();

            return services;
        }
    }
}
