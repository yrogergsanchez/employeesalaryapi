﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.UseCasesPorts.GetOffices;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Presenters
{
    public class GetOfficePresenter : IGetOfficeOutputPort, IPresenter<OfficeViewModel>
    {
        public OfficeViewModel Content { get; private set; }
        public Task Handle(IEnumerable<OfficeDTO> offices)
        {
            Content = new OfficeViewModel(offices);
            return Task.CompletedTask;
        }
    }
}