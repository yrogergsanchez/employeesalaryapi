﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.GetEmployeeByIdentification;
using Employee.Salary.UseCasesPorts.GetEmployeeByIdentification;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Presenters
{
    public class GetEmployeeByIdentificationPresenter : IGetEmployeeByIdentificationOutputPort, IPresenter<EmployeeByIdentificationViewModel>
    {
        public EmployeeByIdentificationViewModel Content { get; private set; }
        public Task Handle(EmployeeByIdentificationDTO employee)
        {
            Content = new EmployeeByIdentificationViewModel(employee);
            return Task.CompletedTask;
        }
    }
}
