﻿using Employee.Salary.UseCasesPorts.CreateEmployee;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Presenters
{
    public class CreateEmployeePresenter : ICreateEmployeeOutputPort, IPresenter<CreateEmployeeViewModel>
    {
        public CreateEmployeeViewModel Content { get; private set; }
        public Task Handle(string employeeIds)
        {
            Content = new CreateEmployeeViewModel(employeeIds);
            return Task.CompletedTask;
        }
    }
}