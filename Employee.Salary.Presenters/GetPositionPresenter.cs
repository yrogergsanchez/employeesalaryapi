﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.UseCasesPorts.GetPositions;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Presenters
{
    public class GetPositionPresenter : IGetPositionOutputPort, IPresenter<PositionViewModel>
    {
        public PositionViewModel Content { get; private set; }
        public Task Handle(IEnumerable<PositionDTO> positions)
        {
            Content = new PositionViewModel(positions);
            return Task.CompletedTask;
        }
    }
}