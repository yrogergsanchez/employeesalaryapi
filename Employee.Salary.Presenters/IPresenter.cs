﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.Presenters
{
    public interface IPresenter<FormatType>
    {
        public FormatType Content { get; }
    }
}
