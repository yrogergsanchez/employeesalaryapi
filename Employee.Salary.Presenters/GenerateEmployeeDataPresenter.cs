﻿using Employee.Salary.UseCasesPorts.GenerateEmployeeData;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Presenters
{
    public class GenerateEmployeeDataPresenter : IGenerateEmployeeDataOutputPort, IPresenter<string>
    {
        public string Content => throw new NotImplementedException();
        public Task Handle()
        {
            return Task.CompletedTask;
        }
    }
}
