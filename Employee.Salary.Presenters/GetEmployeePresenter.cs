﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.UseCasesPorts.CreateEmployee;
using Employee.Salary.UseCasesPorts.GetEmployees;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Presenters
{
    public class GetEmployeePresenter : IGetEmployeesOutputPort, IPresenter<EmployeeViewModel>
    {
        public EmployeeViewModel Content { get; private set; }
        public Task Handle(IEnumerable<EmployeeDTO> employees)
        {
            Content = new EmployeeViewModel(employees);
            return Task.CompletedTask;
        }
    }
}
