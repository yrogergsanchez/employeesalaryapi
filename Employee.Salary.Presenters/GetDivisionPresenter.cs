﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.UseCasesPorts.GetDivisions;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Presenters
{
    public class GetDivisionPresenter : IGetDivisionOutputPort, IPresenter<DivisionViewModel>
    {
        public DivisionViewModel Content { get; private set; }
        public Task Handle(IEnumerable<DivisionDTO> divisions)
        {
            Content = new DivisionViewModel(divisions);
            return Task.CompletedTask;
        }
    }
}
