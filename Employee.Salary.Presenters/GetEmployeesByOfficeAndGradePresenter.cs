﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.UseCasesPorts.GetEmployeeByOfficeAndGrade;
using Employee.Salary.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.Presenters
{
    public class GetEmployeesByOfficeAndGradePresenter : IGetEmployeeByOfficeAndGradeOutputPort, IPresenter<EmployeesViewModel>
    {
        public EmployeesViewModel Content { get; private set; }
        public Task Handle(IEnumerable<EmployeeAllDTO> employees)
        {
            Content = new EmployeesViewModel(employees);
            return Task.CompletedTask;
        }
    }
}
