﻿using Employee.Salary.Entities.POCOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Reflection;

namespace Employee.Salary.EFCore.DataContext
{
    public class EmployeeSalaryContext : DbContext
    {
        public EmployeeSalaryContext(DbContextOptions<EmployeeSalaryContext> options)
           : base(options)
        {
        }

        public DbSet<Employee.Salary.Entities.POCOs.Employee> Employees { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(
               Assembly.GetExecutingAssembly());
        }

    }
}
