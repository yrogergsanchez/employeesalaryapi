﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.EFCore.DataContext.Configurations
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee.Salary.Entities.POCOs.Employee>
    {
        public void Configure(EntityTypeBuilder<Entities.POCOs.Employee> builder)
        {

            builder.ToTable("Empleados");
            builder.HasKey(x => x.Id);

            builder.Property(e => e.BaseSalary)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("Base_Salary");

            builder.Property(e => e.BeginDate)
                .HasColumnType("date")
                .HasColumnName("Begin_Date");

            builder.Property(e => e.Birthday).HasColumnType("date");

            builder.Property(e => e.Commission).HasColumnType("decimal(18, 2)");

            builder.Property(e => e.CompensationBonus)
                .HasColumnType("decimal(18, 2)")
                .HasColumnName("Compensation_Bonus");

            builder.Property(e => e.Contributions).HasColumnType("decimal(18, 2)");

            builder.Property(e => e.Division).HasMaxLength(50);

            builder.Property(e => e.EmployeeCode)
                .IsRequired()
                .HasMaxLength(10)
                .HasColumnName("Employee_Code");

            builder.Property(e => e.EmployeeName)
                .IsRequired()
                .HasMaxLength(150)
                .HasColumnName("Employee_Name");

            builder.Property(e => e.EmployeeSurname)
                .IsRequired()
                .HasMaxLength(150)
                .HasColumnName("Employee_Surname");

            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.IdentificationNumber)
                .IsRequired()
                .HasMaxLength(10)
                .HasColumnName("Identification_Number");

            builder.Property(e => e.Office).HasMaxLength(50);

            builder.Property(e => e.Position).HasMaxLength(50);

            builder.Property(e => e.ProductionBonus)
                .HasColumnType("decimal(18, 2)")
                .HasColumnName("Production_Bonus");           
        }
    }
}