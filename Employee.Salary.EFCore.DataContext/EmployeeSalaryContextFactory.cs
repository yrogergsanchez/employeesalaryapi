﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.EFCore.DataContext
{
    public class EmployeeSalaryContextFactory
        : IDesignTimeDbContextFactory<EmployeeSalaryContext>
    {
        public EmployeeSalaryContext CreateDbContext(string[] args)
        {
            var optionsBuilder =
               new DbContextOptionsBuilder<EmployeeSalaryContext>();
            optionsBuilder.UseSqlServer("Data Source=LAPTOP-V8BRKDUN\\SQL2121;Initial Catalog=Empleados;Integrated Security=True");
            return new EmployeeSalaryContext(optionsBuilder.Options);
        }
    }
}
