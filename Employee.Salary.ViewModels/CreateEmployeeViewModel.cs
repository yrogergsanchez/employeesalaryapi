﻿using System;

namespace Employee.Salary.ViewModels
{
    public class CreateEmployeeViewModel
    {
        public string EmployeeIds { get; set; }
        public CreateEmployeeViewModel(string employeeIds)
        {
            EmployeeIds = employeeIds;
        }
    }
}
