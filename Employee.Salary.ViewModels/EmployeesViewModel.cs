﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.ViewModels
{
    public class EmployeesViewModel
    {
        public IEnumerable<EmployeeAllDTO> Employees { get; set; }
        public EmployeesViewModel(IEnumerable<EmployeeAllDTO> employees)
        {
            Employees = employees;
        }
    }
}
