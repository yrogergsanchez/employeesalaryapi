﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.ViewModels
{
    public class PositionViewModel
    {
        public IEnumerable<PositionDTO> Positions { get; set; }
        public PositionViewModel(IEnumerable<PositionDTO> positions)
        {
            Positions = positions;
        }
    }
}
