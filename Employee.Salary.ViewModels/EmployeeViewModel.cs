﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.ViewModels
{
    public class EmployeeViewModel
    {
        public IEnumerable<EmployeeDTO> Employees { get; set; }
        public EmployeeViewModel(IEnumerable<EmployeeDTO> employees)
        {
            Employees = employees;
        }
    }
}
