﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.ViewModels
{
    public class DivisionViewModel
    {
        public IEnumerable<DivisionDTO> Divisions { get; set; }
        public DivisionViewModel(IEnumerable<DivisionDTO> divisions)
        {
            Divisions = divisions;
        }
    }
}
