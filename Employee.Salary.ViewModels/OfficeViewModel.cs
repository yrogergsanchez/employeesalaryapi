﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.ViewModels
{
    public class OfficeViewModel
    {
        public IEnumerable<OfficeDTO> Offices { get; set; }
        public OfficeViewModel(IEnumerable<OfficeDTO> offices)
        {
            Offices = offices;
        }
    }
}
