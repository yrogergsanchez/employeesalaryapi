﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.GetEmployeeByIdentification;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.ViewModels
{
    public class EmployeeByIdentificationViewModel
    {
        public EmployeeByIdentificationDTO Employee { get; set; }
        public EmployeeByIdentificationViewModel(EmployeeByIdentificationDTO employee)
        {
            Employee = employee;
        }
    }
}