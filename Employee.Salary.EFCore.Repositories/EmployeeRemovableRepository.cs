﻿using Employee.Salary.EFCore.DataContext;
using Employee.Salary.UseCases.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.EFCore.Repositories
{
    public class EmployeeRemovableRepository : IEmployeeRemovableRepository
    {
        readonly EmployeeSalaryContext _context;
        
        public EmployeeRemovableRepository(EmployeeSalaryContext context)
        => (_context) = (context);

        public void DeleteEmployee(Employee.Salary.Entities.POCOs.Employee employee)
        {
            _context.Remove(employee);
        }
    }
}
