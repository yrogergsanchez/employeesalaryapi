﻿using Employee.Salary.EFCore.DataContext;
using Employee.Salary.Entities.Interfaces;
using Employee.Salary.UseCases.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.EFCore.Repositories
{
    public class EmployeeWritableRepository : IEmployeeWritableRepository
    {
        readonly EmployeeSalaryContext _context;
        readonly IRandom _random;
        public EmployeeWritableRepository(EmployeeSalaryContext context,
            IRandom random)
        => (_context) = (context);

        public void CreateEmployee(List<Entities.POCOs.Employee> employees)
        {
            foreach (var item in employees)
            {
                _context.Add(item);
            }
        }
        public void CreateEmployee(Entities.POCOs.Employee employee)
           => _context.Add(employee);

        public void UpdateEmployee(Entities.POCOs.Employee employee)
            => _context.Update(employee);
    }
}
