﻿using Employee.Entities.Interfaces;
using Employee.Salary.DTOs.UpdateEmployee;
using Employee.Salary.Entities.Exceptions;
using Employee.Salary.Entities.Interfaces;
using Employee.Salary.Entities.Validators;
using Employee.Salary.UseCases.Common;
using Employee.Salary.UseCases.Common.Interfaces;
using Employee.Salary.UseCases.Common.Specification;
using Employee.Salary.UseCasesPorts.UpdateEmployee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.UpdateEmployee
{
    public class UpdateEmployeeInteractor : IUpdateEmployeeInputPort
    {
        readonly IEmployeeWritableRepository _employeeRepository;
        readonly IEmployeeReadableRepository _employeeReadableRepository;
        readonly IUnitOfWork _unitOfWork;
        readonly IUpdateEmployeeOutputPort _outputPort;
        readonly IApplicationStatusLogger _logger;
        readonly IEnumerable<Employee.Salary.Entities.Validators.IValidator<UpdateEmployeeDTO>> _validators;
        readonly IValidatorService<UpdateEmployeeDTO> _validatorService;
        readonly IRandom _random;
        public UpdateEmployeeInteractor(IEmployeeWritableRepository employeeRepository,
            IEmployeeReadableRepository employeeReadableRepository,
            IUnitOfWork unitOfWork,
            IUpdateEmployeeOutputPort outputPort,
            IApplicationStatusLogger logger,
            IEnumerable<IValidator<UpdateEmployeeDTO>> validators,
            IValidatorService<UpdateEmployeeDTO> validatorService,
            IRandom random)
        {
            _employeeRepository = employeeRepository;
            _employeeReadableRepository = employeeReadableRepository;
            _unitOfWork = unitOfWork;
            _outputPort = outputPort;
            _logger = logger;
            _validators = validators;
            _validatorService = validatorService;
            _random = random;
        }

        public async Task Handle(List<UpdateEmployeeDTO> dtos)
        {
            foreach (var dto in dtos)
            {
                //var employeesByYearAndMonth = _employeeReadableRepository.GetEmployeesBySpecification(
                //    new EmployeeByYearandMonthSpecification(dto.Year, dto.Month, dto.EmployeeCode), new DTOs.Common.PaginationDTO { Page = 1, Length = 10 });

                //if (employeesByYearAndMonth.Any())
                //{
                //    throw new GeneralException($"No puede guardar mas de una vez el año {dto.Year} y mes {dto.Month} del empleado");
                //}

                _validatorService.Validate(dto, _validators, _logger);
                var employee = dto.MapTo<Employee.Salary.Entities.POCOs.Employee>();

                if (dto.Id > 0)
                {
                    _employeeRepository.UpdateEmployee(employee);
                }
                else
                {
                    var employeeWithCode = dtos.FirstOrDefault(x => !string.IsNullOrEmpty(x.EmployeeCode));
                    if (employeeWithCode == null)
                    {
                        employee.EmployeeCode = _random.GenerateNumber(8);
                    }
                    else
                    {
                        employee.EmployeeCode = employeeWithCode.EmployeeCode;
                    }
                    _employeeRepository.CreateEmployee(employee);
                }
            }

            await _unitOfWork.SaveChanges();                       
            await _outputPort.Handle();
        }
    }
}
