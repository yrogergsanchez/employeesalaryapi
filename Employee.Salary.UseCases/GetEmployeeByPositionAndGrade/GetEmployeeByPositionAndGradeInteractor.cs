﻿using Employee.Salary.DTOs.GetEmployeeByOfficeAndGrade;
using Employee.Salary.DTOs.GetEmployeeByPositionAndGrade;
using Employee.Salary.UseCases.Common.Interfaces;
using Employee.Salary.UseCases.Common.Specification;
using Employee.Salary.UseCasesPorts.GetEmployeeByOfficeAndGrade;
using Employee.Salary.UseCasesPorts.GetEmployeeByPositionAndGrade;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.GetEmployeeByOfficeAndGrade
{
    public class GetEmployeeByPositionAndGradeInteractor : IGetEmployeeByPositionAndGradeInputPort
    {
        readonly IEmployeeReadableRepository _repository;
        readonly IGetEmployeeByPositionAndGradeOutputPort _outport;

        public GetEmployeeByPositionAndGradeInteractor(IEmployeeReadableRepository repository,
            IGetEmployeeByPositionAndGradeOutputPort outport)
        {
            _repository = repository;
            _outport = outport;
        }

        public async Task Handle(EmployeeByPositionAndGradeDTO dto)
        {
            var employeesWithGrades = _repository.GetEmployeesBySpecification(
               new EmployeeByPositionAndGradeSpecification(dto.Grade, dto.Position), dto.Pagination);
            await _outport.Handle(employeesWithGrades);
        }

        
    }
}
