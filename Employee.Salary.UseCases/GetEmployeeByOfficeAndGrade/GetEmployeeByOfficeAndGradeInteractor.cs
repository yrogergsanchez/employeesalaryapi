﻿using Employee.Salary.DTOs.GetEmployeeByOfficeAndGrade;
using Employee.Salary.UseCases.Common.Interfaces;
using Employee.Salary.UseCases.Common.Specification;
using Employee.Salary.UseCasesPorts.GetEmployeeByOfficeAndGrade;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.GetEmployeeByOfficeAndGrade
{
    public class GetEmployeeByOfficeAndGradeInteractor : IGetEmployeeByOfficeAndGradeInputPort
    {
        readonly IEmployeeReadableRepository _repository;
        readonly IGetEmployeeByOfficeAndGradeOutputPort _outport;

        public GetEmployeeByOfficeAndGradeInteractor(IEmployeeReadableRepository repository,
            IGetEmployeeByOfficeAndGradeOutputPort outport)
        {
            _repository = repository;
            _outport = outport;
        }

        public async Task Handle(EmployeeByOfficeAndGradeDTO dto)
        {
            var employeesWithGrades = _repository.GetEmployeesBySpecification(
               new EmployeeByOfficeAndGradeSpecification(dto.Grade, dto.Office), dto.Pagination);
            await _outport.Handle(employeesWithGrades);
        }
    }
}
