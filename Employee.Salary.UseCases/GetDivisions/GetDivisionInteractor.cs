﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.Entities;
using Employee.Salary.Entities.Interfaces;
using Employee.Salary.UseCasesPorts.GetDivisions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.GetDivisions
{
    public class GetDivisionInteractor : IGetDivisionInputPort
    {
        readonly IFileReader _fileReader;
        readonly IGetDivisionOutputPort _outputPort;
        public GetDivisionInteractor(IFileReader fileReader,
            IGetDivisionOutputPort outputPort)
        {
            _fileReader = fileReader;
            _outputPort = outputPort;
        }

        public Task Handle()
        {
            var json = _fileReader.GetFileAsString(Constant.JsonPathDivision);
            var dtos = JsonSerializer.Deserialize<IEnumerable<DivisionDTO>>(json);
            _outputPort.Handle(dtos);
            return Task.CompletedTask;
        }
    }
}
