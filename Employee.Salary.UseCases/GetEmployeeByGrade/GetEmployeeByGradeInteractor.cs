﻿using Employee.Salary.DTOs.GetEmployeeByGrade;
using Employee.Salary.UseCases.Common.Interfaces;
using Employee.Salary.UseCases.Common.Specification;
using Employee.Salary.UseCasesPorts.GetEmployeeByGrade;
using Employee.Salary.UseCasesPorts.GetEmployeeByOfficeAndGrade;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.GetEmployeeByGrade
{
    public class GetEmployeeByGradeInteractor : IGetEmployeeByGradeInputPort
    {
        readonly IEmployeeReadableRepository _repository;
        readonly IGetEmployeeByGradeOutputPort _outport;

        public GetEmployeeByGradeInteractor(IEmployeeReadableRepository repository,
            IGetEmployeeByGradeOutputPort outport)
        {
            _repository = repository;
            _outport = outport;
        }

        public async Task Handle(EmployeeByGradeDTO dto)
        {
            var employeesWithGrades = _repository.GetEmployeesBySpecification(
               new EmployeeByGradeSpecification(dto.Grade), dto.Pagination);
            await _outport.Handle(employeesWithGrades);
        }
    }
}
