﻿using Employee.Salary.UseCases.Common.Interfaces;
using Employee.Salary.UseCasesPorts.DeleteEmployee;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.DeleteEmployee
{
    public class DeleteEmployeeInteractor : IDeleteEmployeeInputPort
    {
        readonly IEmployeeReadableRepository _readableRepository;
        readonly IEmployeeRemovableRepository _removableRepository;
        readonly IDeleteEmployeeOutputPort _outputPort;

        public DeleteEmployeeInteractor(IEmployeeReadableRepository readableRepository, 
            IEmployeeRemovableRepository removableRepository, 
            IDeleteEmployeeOutputPort outputPort)
        {
            _readableRepository = readableRepository;
            _removableRepository = removableRepository;
            _outputPort = outputPort;
        }

        public async Task Handle(int idEmployee)
        {
            var employee = await _readableRepository.GetEmployeeById(idEmployee);
            _removableRepository.DeleteEmployee(employee);
            await _outputPort.Handle();
        }
    }
}
