﻿using Employee.Entities.Interfaces;
using Employee.Salary.DTOs.CreateEmployee;
using Employee.Salary.Entities.Exceptions;
using Employee.Salary.Entities.Interfaces;
using Employee.Salary.Entities.Specification;
using Employee.Salary.Entities.Validators;
using Employee.Salary.UseCases.Common;
using Employee.Salary.UseCases.Common.Interfaces;
using Employee.Salary.UseCases.Common.Specification;
using Employee.Salary.UseCasesPorts.CreateEmployee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.CreateEmployee
{
    public class CreateEmployeeInteractor : ICreateEmployeeInputPort
    {
        readonly IEmployeeWritableRepository _employeeRepository;
        readonly IEmployeeReadableRepository _employeeReadableRepository;
        readonly IUnitOfWork _unitOfWork;
        readonly ICreateEmployeeOutputPort _outputPort;
        readonly IApplicationStatusLogger _logger;
        readonly IEnumerable<Employee.Salary.Entities.Validators.IValidator<CreateEmployeeDTO>> _validators;
        readonly IValidatorService<CreateEmployeeDTO> _validatorService;
        readonly IRandom _random;
        public CreateEmployeeInteractor(IEmployeeWritableRepository employeeRepository,
            IEmployeeReadableRepository employeeReadableRepository,
            IUnitOfWork unitOfWork,
            ICreateEmployeeOutputPort outputPort,
            IApplicationStatusLogger logger,
            IEnumerable<IValidator<CreateEmployeeDTO>> validators,
            IValidatorService<CreateEmployeeDTO> validatorService,
            IRandom random)
        {
            _employeeRepository = employeeRepository;
            _employeeReadableRepository = employeeReadableRepository;
            _unitOfWork = unitOfWork;
            _outputPort = outputPort;
            _logger = logger;
            _validators = validators;
            _validatorService = validatorService;
            _random = random;
        }

        public async Task Handle(List<CreateEmployeeDTO> dtos)
        {
            string employeeCode = _random.GenerateNumber(8);
            foreach (var dto in dtos)
            {                
                dto.EmployeeCode = employeeCode;
                _validatorService.Validate(dto, _validators, _logger);
            }

            var employees = dtos.MapTo<List<Employee.Salary.Entities.POCOs.Employee>>();
            _employeeRepository.CreateEmployee(employees);
            await _unitOfWork.SaveChanges();

            var ids = employees.Select(x => x.Id).ToArray();
            await _outputPort.Handle(string.Join(".", ids));
        }
    }
}
