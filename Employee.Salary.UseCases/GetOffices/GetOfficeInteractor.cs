﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.Entities;
using Employee.Salary.Entities.Interfaces;
using Employee.Salary.UseCasesPorts.GetOffices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.GetOffices
{    
    public class GetOfficeInteractor : IGetOfficeInputPort
    {
        readonly IFileReader _fileReader;
        readonly IGetOfficeOutputPort _outputPort;
        public GetOfficeInteractor(IFileReader fileReader,
            IGetOfficeOutputPort outputPort)
        {
            _fileReader = fileReader;
            _outputPort = outputPort;
        }

        public Task Handle()
        {
            var json = _fileReader.GetFileAsString(Constant.JsonPathOffice);
            var dtos = JsonSerializer.Deserialize<IEnumerable<OfficeDTO>>(json);
            _outputPort.Handle(dtos);
            return Task.CompletedTask;
        }
    }

}
