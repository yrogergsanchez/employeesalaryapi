﻿using Employee.Salary.Entities.Interfaces;
using Employee.Salary.UseCases.Common;
using Employee.Salary.UseCases.CreateEmployee;
using Employee.Salary.UseCases.DeleteEmployee;
using Employee.Salary.UseCases.GenerateEmployeeData;
using Employee.Salary.UseCases.GetDivisions;
using Employee.Salary.UseCases.GetEmployeeByGrade;
using Employee.Salary.UseCases.GetEmployeeByIdentification;
using Employee.Salary.UseCases.GetEmployeeByOfficeAndGrade;
using Employee.Salary.UseCases.GetEmployees;
using Employee.Salary.UseCases.GetOffices;
using Employee.Salary.UseCases.GetPositions;
using Employee.Salary.UseCases.UpdateEmployee;
using Employee.Salary.UseCasesPorts.CreateEmployee;
using Employee.Salary.UseCasesPorts.DeleteEmployee;
using Employee.Salary.UseCasesPorts.GenerateEmployeeData;
using Employee.Salary.UseCasesPorts.GetDivisions;
using Employee.Salary.UseCasesPorts.GetEmployeeByGrade;
using Employee.Salary.UseCasesPorts.GetEmployeeByIdentification;
using Employee.Salary.UseCasesPorts.GetEmployeeByOfficeAndGrade;
using Employee.Salary.UseCasesPorts.GetEmployeeByPositionAndGrade;
using Employee.Salary.UseCasesPorts.GetEmployees;
using Employee.Salary.UseCasesPorts.GetOffices;
using Employee.Salary.UseCasesPorts.GetPositions;
using Employee.Salary.UseCasesPorts.UpdateEmployee;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCases
{
    public static class DependencyContainer
    {
        public static IServiceCollection AddUseCasesServices(
            this IServiceCollection services)
        {
            services.AddTransient<ICreateEmployeeInputPort,
                CreateEmployeeInteractor>();

            services.AddTransient<IUpdateEmployeeInputPort,
               UpdateEmployeeInteractor>();

            services.AddTransient<IDeleteEmployeeInputPort,
              DeleteEmployeeInteractor>();

            services.AddTransient<IGetEmployeesInputPort,
               GetEmployeeInteractor>();

            services.AddTransient<IGetEmployeeByIdentificationInputPort,
             GetEmployeeByIdentificationInteractor>();

            services.AddTransient<IGenerateEmployeeDataInputPort,
               GenerateEmployeeDataInteractor>();

            services.AddTransient<IGetDivisionInputPort,
               GetDivisionInteractor>();

            services.AddTransient<IGetOfficeInputPort,
               GetOfficeInteractor>();

            services.AddTransient<IGetPositionInputPort,
               GetPositionInteractor>();

            services.AddTransient<IGetEmployeeByGradeInputPort,
              GetEmployeeByGradeInteractor>();

            services.AddTransient<IGetEmployeeByOfficeAndGradeInputPort,
              GetEmployeeByOfficeAndGradeInteractor>();

            services.AddTransient<IGetEmployeeByPositionAndGradeInputPort,
              GetEmployeeByPositionAndGradeInteractor>();

            services.AddTransient<IFileReader,
              FileReader>();

            services.AddTransient<IRandom,
               RandomData>();

            return services;
        }
    }
}