﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.Entities;
using Employee.Salary.Entities.Interfaces;
using Employee.Salary.UseCasesPorts.GetPositions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.GetPositions
{   
    public class GetPositionInteractor : IGetPositionInputPort
    {
        readonly IFileReader _fileReader;
        readonly IGetPositionOutputPort _outputPort;
        public GetPositionInteractor(IFileReader fileReader,
            IGetPositionOutputPort outputPort)
        {
            _fileReader = fileReader;
            _outputPort = outputPort;
        }

        public Task Handle()
        {
            var json = _fileReader.GetFileAsString(Constant.JsonPathPosition);
            var dtos = JsonSerializer.Deserialize<IEnumerable<PositionDTO>>(json);
            _outputPort.Handle(dtos);
            return Task.CompletedTask;
        }
    }
}
