﻿using Employee.Entities.Interfaces;
using Employee.Salary.Entities.Interfaces;
using Employee.Salary.UseCases.Common;
using Employee.Salary.UseCases.Common.Interfaces;
using Employee.Salary.UseCasesPorts.GenerateEmployeeData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.GenerateEmployeeData
{
    public class GenerateEmployeeDataInteractor : IGenerateEmployeeDataInputPort
    {
        readonly IEmployeeWritableRepository _employeeRepository;
        readonly IEmployeeReadableRepository _employeeReadableRepository;
        readonly IUnitOfWork _unitOfWork;
        readonly IGenerateEmployeeDataOutputPort _outputPort;
        readonly IApplicationStatusLogger _logger;
        readonly IRandom _random;

        public GenerateEmployeeDataInteractor(IEmployeeWritableRepository employeeRepository,
            IEmployeeReadableRepository employeeReadableRepository,
            IUnitOfWork unitOfWork,
            IGenerateEmployeeDataOutputPort outputPort,
            IApplicationStatusLogger logger,
            IRandom random
           )
        {
            _employeeRepository = employeeRepository;
            _employeeReadableRepository = employeeReadableRepository;
            _unitOfWork = unitOfWork;
            _outputPort = outputPort;
            _logger = logger;
            _random = random;
        }

        public async Task Handle()
        {
            int qty = 100;
            var employees = new List<Employee.Salary.DTOs.CreateEmployee.CreateEmployeeDTO>();
            string[] office = { "A", "C", "D", "ZZ" };
            string[] positions = { "ACCOUNT EXECUTIVE", "CARGO MANAGER", "CUSTOMER ASSISTANT", "MARKETING ASSISTANT", "SALES MANAGER" };
            string[] division = { "CUSTOMER CARE", "MARKETING", "OPERATION", "SALES" };

            while (qty > 0)
            {
                var date = DateTime.Now.AddMonths(-6);
                var employee = new Employee.Salary.DTOs.CreateEmployee.CreateEmployeeDTO
                {
                    EmployeeName = _random.GenerateName(),
                    EmployeeSurname = _random.GenerateName(),
                    BeginDate = date,
                    Birthday = _random.GenerateDate(DateTime.Now.AddYears(-40), DateTime.Now.AddYears(-18)),
                    Month = date.Month,
                    Year = date.Year,
                    BaseSalary = _random.GetAmount(100, 5000),
                    Commission = _random.GetAmount(150, 420),
                    CompensationBonus = _random.GetAmount(150, 500),
                    Contributions = _random.GetAmount(50, 150),
                    ProductionBonus = _random.GetAmount(0, 150),
                    IdentificationNumber = _random.GenerateNumber(10),
                    Grade = (int)_random.GetAmount(6, 18),
                    EmployeeCode = _random.GenerateNumber(8),
                    Division = _random.GetNamesByArrayString(division),
                    Office = _random.GetNamesByArrayString(office),
                    Position = _random.GetNamesByArrayString(positions),
                };

                _employeeRepository.CreateEmployee(employee.MapTo<Employee.Salary.Entities.POCOs.Employee>());

                for (int i = 5; i <= 5; i--)
                {
                    if (i == 0)
                        break;

                    date = DateTime.Now.AddMonths(-i);

                    employee.Year = date.Year;
                    employee.Month = date.Month;
                    employee.BaseSalary = _random.GetAmount(100, 5000);
                    employee.Commission = _random.GetAmount(150, 420);
                    employee.CompensationBonus = _random.GetAmount(150, 500);
                    employee.Contributions = _random.GetAmount(50, 150);
                    employee.ProductionBonus = _random.GetAmount(0, 150);
                    _employeeRepository.CreateEmployee(employee.MapTo<Employee.Salary.Entities.POCOs.Employee>());
                }

                qty--;
            }
                  
            await _unitOfWork.SaveChanges();
            await _outputPort.Handle();
        }
    }
}
