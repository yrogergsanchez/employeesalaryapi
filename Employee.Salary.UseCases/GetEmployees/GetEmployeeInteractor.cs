﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.UseCases.Common.Interfaces;
using Employee.Salary.UseCasesPorts.GetEmployees;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.GetEmployees
{
    public class GetEmployeeInteractor : IGetEmployeesInputPort
    {

        readonly IEmployeeReadableRepository _repository;
        readonly IGetEmployeesOutputPort _outputPort;

        public GetEmployeeInteractor(IEmployeeReadableRepository repository,
            IGetEmployeesOutputPort outputPort)
        {
            _repository = repository;
            _outputPort = outputPort;
        }

        public Task Handle(PaginationDTO pagination)
        {
            var result = _repository.GetEmployees(pagination);
            _outputPort.Handle(result);
            return Task.CompletedTask;
        }
    }
}
