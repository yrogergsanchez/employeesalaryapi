﻿using Employee.Salary.UseCases.Common.Interfaces;
using Employee.Salary.UseCasesPorts.GetEmployeeByIdentification;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.GetEmployeeByIdentification
{
    public class GetEmployeeByIdentificationInteractor : IGetEmployeeByIdentificationInputPort
    {
        readonly IGetEmployeeByIdentificationOutputPort _outport;
        readonly IEmployeeReadableRepository _repository;

        public GetEmployeeByIdentificationInteractor(IGetEmployeeByIdentificationOutputPort outport,
            IEmployeeReadableRepository repository)
        {
            _outport = outport;
            _repository = repository;
        }

        public async Task Handle(string identification)
        {
            var employee = await _repository.GetEmployeeByIdentification(identification);
            await _outport.Handle(employee);
        }
    }
}
