﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.CreateEmployee;
using Employee.Salary.DTOs.GetEmployeeByIdentification;
using Employee.Salary.Entities.Specification;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Employee.Salary.UseCases.Common.Interfaces
{
    public interface IEmployeeReadableRepository
    {
        Task<Employee.Salary.Entities.POCOs.Employee> GetEmployeeById(int idEmployee);
        Task<EmployeeByIdentificationDTO> GetEmployeeByIdentification(string identification);
        IEnumerable<EmployeeDTO> GetAllEmployees();
        IEnumerable<EmployeeDTO> GetEmployees(PaginationDTO pagination);
        IEnumerable<EmployeeAllDTO> GetEmployeesBySpecification(
            Specification<EmployeeAllDTO> specification, PaginationDTO paginationDTO);        
    }
}