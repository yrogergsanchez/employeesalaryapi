﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCases.Common.Interfaces
{
    public interface IEmployeeRemovableRepository
    {
        void DeleteEmployee(Employee.Salary.Entities.POCOs.Employee employee);
    }
}
