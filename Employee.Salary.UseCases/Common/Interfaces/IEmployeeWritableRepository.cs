﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Employee.Salary.UseCases.Common.Interfaces
{
    public interface IEmployeeWritableRepository
    {
        void CreateEmployee(List<Entities.POCOs.Employee> employees);
        void CreateEmployee(Entities.POCOs.Employee employee);
        void UpdateEmployee(Entities.POCOs.Employee employee);
    }
}