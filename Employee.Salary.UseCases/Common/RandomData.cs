﻿using Employee.Salary.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Employee.Salary.UseCases.Common
{
    public class RandomData : IRandom
    {
        readonly Random _random;
        public RandomData()
        {
            _random = new Random();
        }

        public string GenerateName()
        {
            int len = _random.Next(5, 10);
            string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", "n", "p", "q", "r", "s", "sh", "zh", "t", "v", "w", "x" };
            string[] vowels = { "a", "e", "i", "o", "u", "ae", "y" };
            string Name = "";
            Name += consonants[_random.Next(consonants.Length)].ToUpper();
            Name += vowels[_random.Next(vowels.Length)];
            int b = 2;
            while (b < len)
            {
                Name += consonants[_random.Next(consonants.Length)];
                b++;
                Name += vowels[_random.Next(vowels.Length)];
                b++;
            }
            return Name;
        }

        public string GenerateNumber(int len)
        {
            string[] numbers = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            string number = "";
            number += numbers[_random.Next(numbers.Length)].ToUpper();

            int b = 1;
            while (b < len)
            {
                number += numbers[_random.Next(numbers.Length)];
                b++;
            }
            return number;
        }

        public DateTime GenerateDate(DateTime start, DateTime end)
        {
            int range = ((TimeSpan)(end - start)).Days;
            var date = start.AddDays(_random.Next(range));
            return date;
        }

        public string GetNamesByArrayString(string[] arrays)
        => arrays[_random.Next(0, arrays.Length)];

        public IList<DateTime> GetRandomDates(DateTime startDate, DateTime maxDate, int range)
        {
            var randomResult = GetRandomNumbers(range).ToArray();

            var calculationValue = maxDate.Subtract(startDate).TotalMinutes / int.MaxValue;
            var dateResults = randomResult.Select(s => startDate.AddMinutes(s * calculationValue)).ToList();
            return dateResults;
        }

        public int GetIndex(int length)
            => _random.Next(length);

        public decimal GetAmount()
            => new decimal(_random.NextDouble());

        public decimal GetAmount(int min, int max)
            => _random.Next(min, max);


        private IEnumerable<int> GetRandomNumbers(int size)
        {
            var data = new byte[4];
            using (var rng = new System.Security.Cryptography.RNGCryptoServiceProvider(data))
            {
                for (int i = 0; i < size; i++)
                {
                    rng.GetBytes(data);

                    var value = BitConverter.ToInt32(data, 0);
                    yield return value < 0 ? value * -1 : value;
                }
            }
        }

    }
}
