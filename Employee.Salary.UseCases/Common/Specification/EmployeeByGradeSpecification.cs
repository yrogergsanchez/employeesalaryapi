﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Employee.Salary.UseCases.Common.Specification
{
    public class EmployeeByGradeSpecification 
        : Employee.Salary.Entities.Specification.Specification<Employee.Salary.DTOs.Common.EmployeeAllDTO>
    {
        readonly int _grade;
        public EmployeeByGradeSpecification(int grade)
        {
            _grade = grade;
        }
        public override Expression<Func<EmployeeAllDTO, bool>> ConditionExpression =>
            employee => employee.Grade == _grade;
    }
}
