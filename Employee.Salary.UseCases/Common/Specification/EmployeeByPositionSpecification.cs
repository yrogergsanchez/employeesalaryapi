﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Employee.Salary.UseCases.Common.Specification
{
    public class EmployeeByPositionSpecification
        : Employee.Salary.Entities.Specification.Specification<Employee.Salary.DTOs.Common.EmployeeAllDTO>
    {
        readonly string _position;
        public EmployeeByPositionSpecification(string position)
        {
            _position = position;
        }
        public override Expression<Func<EmployeeAllDTO, bool>> ConditionExpression =>
            employee => employee.Position == _position;
    }
}
