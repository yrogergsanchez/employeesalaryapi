﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Employee.Salary.UseCases.Common.Specification
{
    public class EmployeeByOfficeSpecification
        : Employee.Salary.Entities.Specification.Specification<Employee.Salary.DTOs.Common.EmployeeAllDTO>
    {
        readonly string _office;
        public EmployeeByOfficeSpecification(string office)
        {
            _office = office;
        }
        public override Expression<Func<EmployeeAllDTO, bool>> ConditionExpression =>
            employee => employee.Office == _office;
    }
}
