﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.DTOs.CreateEmployee;
using Employee.Salary.Entities.Specification;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Employee.Salary.UseCases.Common.Specification
{
    public class EmployeeByYearandMonthSpecification : Specification<EmployeeAllDTO>
    {
        readonly int _year;
        readonly int _month;
        readonly string _employeeCode;
        public EmployeeByYearandMonthSpecification(int year, int month, string employeeCode)
        {
            _year = year;
            _month = month;
            _employeeCode = employeeCode;
        }
        public override Expression<Func<EmployeeAllDTO, bool>> ConditionExpression
        {
            get
            {                
                var yearSpecificacion = new EmployeeByYearSpecification(_year);
                var monthSpecificacion = new EmployeeByMonthSpecification(_month);
                var employeebyCodeSpecification = new EmployeeByCodeSpecification(_employeeCode);
                return yearSpecificacion.And(monthSpecificacion).And(employeebyCodeSpecification).ConditionExpression;
            }
        }        
    }
}
