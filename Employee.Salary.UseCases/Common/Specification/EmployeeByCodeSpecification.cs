﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Employee.Salary.UseCases.Common.Specification
{
    public class EmployeeByCodeSpecification : Employee.Salary.Entities.Specification.Specification<Employee.Salary.DTOs.Common.EmployeeAllDTO>
    {
        readonly string _employeeCode;
        public EmployeeByCodeSpecification(string employeeCode)
        {
            _employeeCode = employeeCode;
        }
        public override Expression<Func<EmployeeAllDTO, bool>> ConditionExpression =>
            employee => employee.EmployeeCode == _employeeCode;
    }
}
