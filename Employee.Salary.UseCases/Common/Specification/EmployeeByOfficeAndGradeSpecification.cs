﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.Entities.Specification;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Employee.Salary.UseCases.Common.Specification
{
    public class EmployeeByOfficeAndGradeSpecification : Specification<EmployeeAllDTO>
    {
        readonly int _grade;
        readonly string _office;
        public EmployeeByOfficeAndGradeSpecification(int grade, string office)
        {
            _grade = grade;
            _office = office;
        }
        public override Expression<Func<EmployeeAllDTO, bool>> ConditionExpression
        {
            get
            {
                var employeeByGradeSpecificacion = new EmployeeByGradeSpecification(_grade);
                var employeeByOfficeSpecification = new EmployeeByOfficeSpecification(_office);
                return employeeByGradeSpecificacion.And(employeeByOfficeSpecification).ConditionExpression;
            }
        }
    }
}
