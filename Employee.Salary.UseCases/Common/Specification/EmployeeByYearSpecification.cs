﻿using Employee.Salary.DTOs.Common;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Employee.Salary.UseCases.Common.Specification
{
    public class EmployeeByYearSpecification : Employee.Salary.Entities.Specification.Specification<Employee.Salary.DTOs.Common.EmployeeAllDTO>
    {
        readonly int _year;
        public EmployeeByYearSpecification(int year)
        {
            _year = year;
        }
        public override Expression<Func<EmployeeAllDTO, bool>> ConditionExpression =>
            employee => employee.Year == _year;
    }
}
