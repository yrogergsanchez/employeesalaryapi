﻿using Employee.Salary.DTOs.Common;
using Employee.Salary.Entities.Specification;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Employee.Salary.UseCases.Common.Specification
{
    public class EmployeeByPositionAndGradeSpecification : Specification<EmployeeAllDTO>
    {
        readonly int _grade;
        readonly string _position;
        public EmployeeByPositionAndGradeSpecification(int grade, string position)
        {
            _grade = grade;
            _position = position;
        }
        public override Expression<Func<EmployeeAllDTO, bool>> ConditionExpression
        {
            get
            {
                var employeeByGradeSpecificacion = new EmployeeByGradeSpecification(_grade);
                var employeeByPositionSpecification = new EmployeeByPositionSpecification(_position);
                return employeeByGradeSpecificacion.And(employeeByPositionSpecification).ConditionExpression;
            }
        }
    }
}
