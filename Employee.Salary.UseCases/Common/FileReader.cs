﻿using Employee.Salary.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Employee.Salary.UseCases.Common
{
    public class FileReader : IFileReader
    {
        public string GetFileAsString(string filepath)
        {
            if (!File.Exists(filepath))
                throw new FileNotFoundException();

            return File.ReadAllText(filepath);
        }
    }
}
