﻿using Employee.Entities.Interfaces;
using Employee.Salary.Dapper;
using Employee.Salary.EFCore.DataContext;
using Employee.Salary.EFCore.Repositories;
using Employee.Salary.UseCases.Common.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Data;

namespace Employee.Salary.Repository.IoC
{
    public static class DependencyContainer
    {
        public static IServiceCollection AddRepositories(
           this IServiceCollection services,
           IConfiguration configuration,
           string connectionStringName)
        {
            services.AddDbContext<EmployeeSalaryContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString(connectionStringName)));

            services.AddScoped<IEmployeeWritableRepository,
                EmployeeWritableRepository>();
                      
            services.AddScoped<IDbConnection>(provider =>
            new SqlConnection(configuration.GetConnectionString(connectionStringName)));

            services.AddScoped<IEmployeeReadableRepository, EmployeeReadableRepository>();
            services.AddScoped<IEmployeeRemovableRepository, EmployeeRemovableRepository>();

            services.AddScoped<IUnitOfWork,
                UnitOfWork>();

            return services;
        }
    }
}
